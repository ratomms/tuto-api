
PREREQUIS POUR FAIRE FONCTIONER UN API AVEC SLIM PHP
-----------------------------------------------------

=> Changes in apache2.conf
--------------------------

1. Get the path of running Apache
```bash
    ps -ef | grep apache
```
   Append -V argument to the path
```bash
    /usr/sbin/apache2 -V | grep SERVER_CONFIG_FILE
```

2. Naviagte to apache2.conf
```bash
    nano /etc/apache2/apache2.conf
```
3. Update the file Replace "AllowOverride None" to "AllowOverride All"
```bash
    <Directory /var/www/>
            Options Indexes FollowSymLinks
            AllowOverride All
            Require all granted
    </Directory>
```
4. Restart apache2 after
```bash
    service apache2 restart
```
   OR
```bash
    apachectl -k graceful
```

=> Enable mod_rewrite for Apache 2.2
------------------------------------

1. Type the following command in the terminal:
```bash
    a2enmod rewrite
```

2. Restart apache2 after
```bash
    service apache2 restart
```

QUELQUES COMMANDES GIT POUR UN PROJET
-------------------------------------
1. Initialiser un repository git:
```bash
    git init
```

2. Ajouter tous les fichiers dans le repository:
```bash
    git add .
```

3. Commiter les fichiers dans le repository:
```bash
    git commit -m "Initial commit"
```

4. Ajouter un repository distant(Gitlab):
```bash
    git remote add origin git@gitlab.com:ratomms/repo_name.git
```
    Exemple:
```bash
    git remote add origin git@gitlab.com:ratomms/tuto-api.git
```

5. Envoyer les fichiers commité dans le repository distant:
```bash
    git push -u origin master
```

6. Apres la commande ci-dessus, on peut faire tout simplement :
```bash
    git push
```
    
    Parce que l'option -u ici dit à Git que c'est le repot distant par défaut à utiliser
7. Créer un nouvelle branche et faire ceci le current branch
```bash
    git checkout -b branch_name
```




