<?php
require 'vendor/autoload.php';
include 'config.php';
$app = new Slim\App(["settings" => $config, 'mode' => 'development', 'debug' => true]);

//Handle Dependencies
$container = $app->getContainer();

$container['db'] = function ($c) {
   
   try{
       $db = $c['settings']['db'];
       $options  = array(
           PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
           PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
       );
       $pdo = new PDO("mysql:host=" . $db['servername'] . ";dbname=" . $db['dbname'],
       $db['username'], $db['password'],$options);
       return $pdo;
   }
   catch(\Exception $ex){
       return $ex->getMessage();
   }
   
};
// Create users API
$app->post('/user', function ($request, $response) {
   
    try{
        $con = $this->db;
        $sql = "INSERT INTO `users`(`USERNAME`, `EMAIL`,`PASSWORD`) VALUES (:username,:email,:pwd)";
        $pre  = $con->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $values = array(
            ':username' => $request->getParam('username'),
            ':email' => $request->getParam('email'),
            //Using hash for password encryption
            ':pwd' => password_hash($request->getParam('pwd'),PASSWORD_DEFAULT)
        );
        
        $result = $pre->execute($values);
        
        return $response->withJson(array('status' => 'User Created'),200);
        
    }
    catch(\Exception $ex){
        return $response->withJson(array('error' => $ex->getMessage()),422);
    }
    
 });

 // Retrieve one user information API
 $app->get('/user/{id}', function ($request,$response) {
    try{
        $id     = $request->getAttribute('id');
        $con = $this->db;
        $sql = "SELECT * FROM users WHERE ID = :id";
        $pre  = $con->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $values = array(
            ':id' => $id
        );
        $pre->execute($values);
        $result = $pre->fetch();
        if($result){
            return $response->withJson(array('status' => 'true','result'=> $result),200);
        }else{
            return $response->withJson(array('status' => 'User Not Found'),422);
        }
       
    }
    catch(\Exception $ex){
        return $response->withJson(array('error' => $ex->getMessage()),422);
    }
    
 });
 
 // Retrieve all recorded users API
 $app->get('/users', function ($request,$response) {
    try{
        $con = $this->db;
        $sql = "SELECT * FROM users";
        $result = null;
        foreach ($con->query($sql) as $row) {
            $result[] = $row;
        }
        if($result){
            return $response->withJson(array('status' => 'true','result'=>$result),200);
        }else{
            return $response->withJson(array('status' => 'Users Not Found'),422);
        }
               
    }
    catch(\Exception $ex){
        return $response->withJson(array('error' => $ex->getMessage()),422);
    }
    
 });

 // Updates one user record API
 $app->put('/user/{id}', function ($request,$response) {
    try{
        $id     = $request->getAttribute('id');
        $con = $this->db;
        $sql = "UPDATE users SET USERNAME=:username,EMAIL=:email,PASSWORD=:pwd WHERE ID = :id";
        $pre  = $con->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $values = array(
            ':username' => $request->getParam('username'),
            ':email' => $request->getParam('email'),
            ':pwd' => password_hash($request->getParam('pwd'),PASSWORD_DEFAULT),
            ':id' => $id
        );
        $result =  $pre->execute($values);
        if($result){
            return $response->withJson(array('status' => 'User Updated'),200);
        }else{
            return $response->withJson(array('status' => 'User Not Found'),422);
        }
               
    }
    catch(\Exception $ex){
        return $response->withJson(array('error' => $ex->getMessage()),422);
    }
    
 });

 // Delete one user record API
 $app->delete('/user/{id}', function ($request,$response) {
    try{
        $id     = $request->getAttribute('id');
        $con = $this->db;
        $sql = "DELETE FROM users WHERE ID = :id";
        $pre  = $con->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $values = array(
        ':id' => $id);
        $result = $pre->execute($values);
        if($result){
            return $response->withJson(array('status' => 'User Deleted'),200);
        }else{
            return $response->withJson(array('status' => 'User Not Found'),422);
        }
       
    }
    catch(\Exception $ex){
        return $response->withJson(array('error' => $ex->getMessage()),422);
    }
    
 });

 // Run the application
 $app->run();